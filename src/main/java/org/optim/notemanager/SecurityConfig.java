package org.optim.notemanager;

import javax.inject.Inject;

import org.optim.notemanager.database.service.DatabaseService;
import org.optim.notemanager.domain.security.AuthenticationFilter;
import org.optim.notemanager.domain.security.LoginFailureHandler;
import org.optim.notemanager.domain.security.LoginFilter;
import org.optim.notemanager.domain.security.LoginSuccessHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Value("${api.endpoint.register}")
	private String ENDPOINT_REGISTER;
	@Value("${api.endpoint.login}")
	private String ENDPOINT_LOGIN;

	@Inject
	private ObjectMapper mapper;
	@Inject
	DatabaseService databaseService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().antMatchers(HttpMethod.POST, ENDPOINT_REGISTER).permitAll()
				.antMatchers(HttpMethod.POST, ENDPOINT_LOGIN).permitAll().anyRequest().authenticated().and()
				.addFilterBefore(loginFilter(), UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(databaseService).passwordEncoder(encoder());
	}

	@Bean
	public AuthenticationFilter authenticationFilter() {
		return new AuthenticationFilter();
	}

	@Bean
	public LoginFilter loginFilter() throws Exception {
		return new LoginFilter(ENDPOINT_LOGIN, authenticationManager(), mapper, loginSuccessHandler(),
				loginFailureHandler());
	}

	@Bean
	public LoginSuccessHandler loginSuccessHandler() {
		return new LoginSuccessHandler();
	}

	@Bean
	public LoginFailureHandler loginFailureHandler() {
		return new LoginFailureHandler(mapper);
	}

	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}
}