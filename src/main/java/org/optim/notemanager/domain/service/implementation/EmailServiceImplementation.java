package org.optim.notemanager.domain.service.implementation;

import java.nio.charset.StandardCharsets;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.optim.notemanager.domain.model.Email;
import org.optim.notemanager.domain.service.EmailService;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

@Service
public class EmailServiceImplementation implements EmailService {
	@Inject
	private JavaMailSender sender;
	@Inject
	private SpringTemplateEngine templateEngine;

	@Override
	public void send(Email email) {
		try {
			Context context = new Context();
			context.setVariables(email.getModel());
			String html = templateEngine.process("email-template", context);
			
			MimeMessage message = sender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message,
															 MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
															 StandardCharsets.UTF_8.name());
			helper.setTo(email.getTo());
			helper.setText(html, true);
			helper.setSubject(email.getSubject());
			helper.setFrom(email.getFrom());
			
			sender.send(message);
		} catch (MailException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}