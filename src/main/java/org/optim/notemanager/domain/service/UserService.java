package org.optim.notemanager.domain.service;

import org.optim.notemanager.domain.model.User;

public interface UserService {

	void createUser(User user);
}