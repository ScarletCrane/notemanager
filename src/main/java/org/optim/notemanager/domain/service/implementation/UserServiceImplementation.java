package org.optim.notemanager.domain.service.implementation;

import javax.inject.Inject;

import org.optim.notemanager.database.entity.UserEntity;
import org.optim.notemanager.database.repository.UserRepository;
import org.optim.notemanager.domain.model.User;
import org.optim.notemanager.domain.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImplementation implements UserService {
	@Inject
	private UserRepository userRepository;

	@Override
	public void createUser(User user) {

		userRepository.save(new UserEntity(user.getUsername(), user.getPassword(), user.getEmail()));
	}
}