package org.optim.notemanager.domain.service;

import org.optim.notemanager.domain.model.Email;

public interface EmailService {
	void send(Email email);
}