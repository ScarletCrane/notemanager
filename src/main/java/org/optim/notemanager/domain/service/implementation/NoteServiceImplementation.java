package org.optim.notemanager.domain.service.implementation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.optim.notemanager.database.entity.NoteEntity;
import org.optim.notemanager.database.entity.UserEntity;
import org.optim.notemanager.database.repository.NoteRepository;
import org.optim.notemanager.database.repository.UserRepository;
import org.optim.notemanager.domain.model.Email;
import org.optim.notemanager.domain.model.Note;
import org.optim.notemanager.domain.service.EmailService;
import org.optim.notemanager.domain.service.NoteService;
import org.springframework.stereotype.Service;

@Service
public class NoteServiceImplementation implements NoteService {
	@Inject
	private NoteRepository noteRepository;
	@Inject
	private UserRepository userRepository;
	@Inject
	private EmailService emailService;
	
	private static String OPERATION;

	@Override
	public Long addNote(String username, String content) {
		OPERATION = "ADDED";

		UserEntity user = userRepository.findByUsername(username);

		NoteEntity note = new NoteEntity(user, content);
		NoteEntity result = noteRepository.save(note);

		sendEmail(result.getId(), result.getText(), result.getUser().getUsername(), result.getUser().getEmail());

		return result.getId();
	}

	@Override
	public void updateNote(String username, Long noteId, String content) {
		OPERATION = "UPDATED";

		UserEntity user = userRepository.findByUsername(username);
		for (NoteEntity note : user.getNotes()) {
			if (noteId.equals(note.getId())) {
				noteRepository.update(content, noteId);
				sendEmail(note.getId(), content, user.getUsername(), user.getEmail());
				break;
			}
		}
	}

	@Override
	public void deleteNote(String username, Long noteId) {
		OPERATION = "DELETED";
		
		UserEntity user = userRepository.findByUsername(username);
		for (NoteEntity note : user.getNotes()) {
			if (noteId.equals(note.getId())) {
				noteRepository.deleteById(noteId);
				sendEmail(note.getId(), note.getText(), user.getUsername(), user.getEmail());
				break;
			}
		}
	}

	@Override
	public List<Note> getNotes(String username) {
		UserEntity user = userRepository.findByUsername(username);

		List<Note> notes = new ArrayList<Note>();
		for (NoteEntity note : user.getNotes()) {
			notes.add(new Note(note.getId(), note.getText()));
		}

		return notes;
	}
	
	private void sendEmail(Long id, String text, String username, String address) {
		Email email = new Email();
		email.setFrom("no-reply@optim.com.tr");
		email.setTo(address);
		email.setSubject("Note Manager Notifier");

		Map<String, Object> model = new HashMap<>();
		model.put("name", username);
		model.put("operation", OPERATION);
		model.put("noteId", id);
		model.put("content", text);
		model.put("signature", "https://www.optim.com.tr");
		model.put("location", "Turkey");
		email.setModel(model);

		emailService.send(email);
	}
}