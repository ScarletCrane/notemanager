package org.optim.notemanager.domain.service;

import java.util.List;

import org.optim.notemanager.domain.model.Note;

public interface NoteService {
	Long addNote(String username, String content);

	void updateNote(String username, Long noteId, String content);

	void deleteNote(String username, Long noteId);

	List<Note> getNotes(String username);
}