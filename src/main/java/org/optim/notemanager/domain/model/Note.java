package org.optim.notemanager.domain.model;

public class Note {
	private Long id;
	private String text;
	
	public Note(Long id, String text) {
		setId(id);
		setText(text);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}