package org.optim.notemanager.domain.model;

import java.util.List;
import java.util.Map;

public class Email {
	private String from;
	private String to;
	private String subject;
	private String text;
	private List<Object> attachments;
	private Map<String, Object> model;

	public Email() {
	}

	public Email(String to, String subject, String text) {
		setTo(to);
		setSubject(subject);
		setText(text);
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<Object> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Object> attachments) {
		this.attachments = attachments;
	}

	public Map<String, Object> getModel() {
		return model;
	}

	public void setModel(Map<String, Object> model) {
		this.model = model;
	}
}