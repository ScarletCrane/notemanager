package org.optim.notemanager.domain.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.optim.notemanager.database.entity.UserEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class LoginFilter extends AbstractAuthenticationProcessingFilter {
	private final ObjectMapper mapper;
	private final LoginSuccessHandler successHandler;
	private final LoginFailureHandler failureHandler;

	public LoginFilter(String url, AuthenticationManager manager, ObjectMapper mapper, LoginSuccessHandler successHandler, LoginFailureHandler failureHandler) {
		super(new AntPathRequestMatcher(url));

		setAuthenticationManager(manager);

		this.mapper = mapper;
		this.successHandler = successHandler;
		this.failureHandler = failureHandler;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		UserEntity user = mapper.readValue(request.getInputStream(), UserEntity.class);
		if (StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getPassword()))
			throw new AuthenticationServiceException("Username or Password not provided!");

		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getUsername(),
				user.getPassword());
		return getAuthenticationManager().authenticate(token);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult)
			throws IOException, ServletException {
		successHandler.onAuthenticationSuccess(request, response, authResult);
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed)
			throws IOException, ServletException {
		SecurityContextHolder.clearContext();
		
		failureHandler.onAuthenticationFailure(request, response, failed);
	}
}