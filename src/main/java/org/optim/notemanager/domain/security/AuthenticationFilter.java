package org.optim.notemanager.domain.security;

import java.io.IOException;
import java.util.Collections;

import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.optim.notemanager.database.entity.UserEntity;
import org.optim.notemanager.database.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import io.jsonwebtoken.Jwts;

public class AuthenticationFilter extends GenericFilterBean {
	@Value("${api.jwt.header}")
	private String HEADER;
	@Value("${api.jwt.prefix}")
	private String PREFIX;
	@Value("${api.jwt.secret}")
	private String SECRET;
	
	@Inject
	private UserRepository userRepository;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		Authentication authentication = getAuthentication((HttpServletRequest) request);
		SecurityContextHolder.getContext().setAuthentication(authentication);

		chain.doFilter(request, response);
	}

	private Authentication getAuthentication(HttpServletRequest request)
			throws IOException {
		String jwtToken = request.getHeader(HEADER);
		if (jwtToken == null)
			return null;
		
		String username = Jwts.parser().setSigningKey(SECRET)
								   .parseClaimsJws(jwtToken.replace(PREFIX, ""))
								   .getBody().getSubject();
		if (username == null)
			throw new IOException("Username from token is missing!");
		
		UserEntity entity = userRepository.findByUsername(username);
		if (entity == null)
			throw new IOException("Username from token doesn't exist in any of the users on db!");
		
		return new UsernamePasswordAuthenticationToken(username, null, Collections.emptyList());
	}
}