package org.optim.notemanager.domain.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class LoginSuccessHandler implements AuthenticationSuccessHandler {
	@Value("${api.jwt.header}")
	private String HEADER;
	@Value("${api.jwt.prefix}")
	private String PREFIX;
	@Value("${api.jwt.secret}")
	private String SECRET;
	@Value("${api.jwt.expiration}")
	private int EXPIRATION;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		String username = (String) authentication.getName();
		
		String jwtToken = Jwts.builder().setSubject(username)
										.setExpiration(new Date(System.currentTimeMillis() + (EXPIRATION * 60 * 1000)))
										.signWith(SignatureAlgorithm.HS512, SECRET).compact();
		
		response.setStatus(HttpStatus.OK.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.addHeader(HEADER, PREFIX + " " + jwtToken);
	}
	
	protected final void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null)
			return;

		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}
}