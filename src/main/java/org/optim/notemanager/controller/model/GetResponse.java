package org.optim.notemanager.controller.model;

import java.util.List;

import org.optim.notemanager.domain.model.Note;

public class GetResponse {
	private List<Note> notes;

	public GetResponse(List<Note> notes) {
		setNotes(notes);
	}

	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}
}