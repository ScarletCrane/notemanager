package org.optim.notemanager.controller.model;

public class AddResponse {
	private Long id;

	public AddResponse(Long id) {
		setId(id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}