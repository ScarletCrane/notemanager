package org.optim.notemanager.controller.model;

import javax.validation.constraints.NotNull;

public class DeleteRequest {
	@NotNull
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}