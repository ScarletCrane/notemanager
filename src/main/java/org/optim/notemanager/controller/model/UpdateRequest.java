package org.optim.notemanager.controller.model;

import javax.validation.constraints.NotNull;

public class UpdateRequest {
	@NotNull
	private Long id;
	@NotNull
	private String text;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}