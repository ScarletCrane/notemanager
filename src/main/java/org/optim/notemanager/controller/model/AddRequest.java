package org.optim.notemanager.controller.model;

import javax.validation.constraints.NotNull;

public class AddRequest {
	@NotNull
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}