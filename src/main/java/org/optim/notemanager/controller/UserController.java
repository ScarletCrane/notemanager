package org.optim.notemanager.controller;

import javax.inject.Inject;

import org.optim.notemanager.controller.model.RegisterRequest;
import org.optim.notemanager.domain.model.User;
import org.optim.notemanager.domain.service.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/notemanager")
public class UserController {
	@Inject
	private UserService userService;

	@PostMapping("/register")
	public void register(@RequestBody RegisterRequest request) {

		userService.createUser(new User(request.getUsername(), request.getPassword(), request.getEmail()));
	}
}