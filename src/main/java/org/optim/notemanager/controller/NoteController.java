package org.optim.notemanager.controller;

import java.security.Principal;
import java.util.List;

import javax.inject.Inject;

import org.optim.notemanager.controller.model.AddRequest;
import org.optim.notemanager.controller.model.AddResponse;
import org.optim.notemanager.controller.model.DeleteRequest;
import org.optim.notemanager.controller.model.GetResponse;
import org.optim.notemanager.controller.model.UpdateRequest;
import org.optim.notemanager.domain.model.Note;
import org.optim.notemanager.domain.service.NoteService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/notemanager/note")
public class NoteController {
	@Inject
	private NoteService noteService;

	@PostMapping("/add")
	public AddResponse add(@RequestBody AddRequest request, Principal user) {
		Long noteId = noteService.addNote(user.getName(), request.getText());
		return new AddResponse(noteId);
	}

	@PostMapping("/update")
	public void update(@RequestBody UpdateRequest request, Principal user) {
		noteService.updateNote(user.getName(), request.getId(), request.getText());
	}

	@PostMapping("/delete")
	public void delete(@RequestBody DeleteRequest request, Principal user) {
		noteService.deleteNote(user.getName(), request.getId());
	}

	@PostMapping("/get")
	public GetResponse get(Principal user) {
		List<Note> notes = noteService.getNotes(user.getName());
		return new GetResponse(notes);
	}
}