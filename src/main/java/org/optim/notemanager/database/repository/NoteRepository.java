package org.optim.notemanager.database.repository;

import org.optim.notemanager.database.entity.NoteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface NoteRepository extends JpaRepository<NoteEntity, Long> {

	@Modifying
	@Transactional
	@Query("UPDATE NoteEntity n SET n.text = :text WHERE n.id = :noteId")
	void update(@Param("text") String text, @Param("noteId") Long noteId);
	
	@Modifying
	@Transactional
	void deleteById(Long id);
}