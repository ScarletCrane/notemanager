package org.optim.notemanager.database.service;

import javax.inject.Inject;

import org.optim.notemanager.database.entity.UserEntity;
import org.optim.notemanager.database.repository.UserRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class DatabaseService implements UserDetailsService {
	@Inject
	private UserRepository userRepository;
	@Inject
	private BCryptPasswordEncoder encoder;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserEntity user = userRepository.findByUsername(username);
		if (user == null)
			throw new UsernameNotFoundException(username);
		
		UserBuilder builder = User.withUsername(username);
		builder.password(encoder.encode(user.getPassword()));
		builder.roles("USER");
		return builder.build();
	}
}