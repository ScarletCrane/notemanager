package org.optim.notemanager.domain.service;

import javax.inject.Inject;

import org.junit.Test;
import org.optim.notemanager.BaseUnitTest;
import org.optim.notemanager.domain.model.User;

public class UserServiceTest extends BaseUnitTest {
	@Inject
	private UserService userService;

	@Test
	public void createUser() {
		User user = new User("Lena", "123456", "perevertun_lena@mail.ru");
		userService.createUser(user);
	}
}