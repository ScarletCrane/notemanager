package org.optim.notemanager.database.repository;

import javax.inject.Inject;

import org.junit.Test;
import org.optim.notemanager.BaseUnitTest;
import org.optim.notemanager.database.entity.UserEntity;

public class UserRepositoryUnitTest extends BaseUnitTest {
	@Inject
	private UserRepository userRepository;

	@Test
	public void save() {

		userRepository.save(new UserEntity("Lena", "123456", "perevertun_lena@mail.ru"));
	}
}